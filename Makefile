presentation.tex: presentation.org
	emacs presentation.org --batch -l org-files-to-tex.el

presentation.pdf: presentation.tex
	latexmk --pdflatex -shell-escape -interaction=nonstopmode presentation.tex

# presentation.pdf: presentation.org
# 	emacs presentation.org --batch -l org-files-to-tex.el

.PHONY: clean
clean:
	rm -f presentation.*.vrb presentation.aux presentation.dvi presentation.fdb_latexmk presentation.fls presentation.log presentation.out presentation.pdf presentation.toc presentation.snm presentation.nav

.PHONY: runs
runs:
	(cd runs/external_current/ && octopus)
	cp runs/optical_spectra_from_casida/inp_gs runs/optical_spectra_from_casida/inp
	(cd runs/optical_spectra_from_casida/ && octopus)
	cp runs/optical_spectra_from_casida/inp_unocc runs/optical_spectra_from_casida/inp
	(cd runs/optical_spectra_from_casida/ && octopus)
	cp runs/optical_spectra_from_casida/inp_casida runs/optical_spectra_from_casida/inp
	(cd runs/optical_spectra_from_casida/ && octopus)
